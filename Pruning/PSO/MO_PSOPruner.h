/*
 * MO_PSO.h
 *
 *  Created on: Nov 12, 2010
 *      Author: rgreen
 */

#ifndef MO_PSOPRUNER_H_
#define MO_PSOPRUNER_H_

#include "MOPruner.h"
#include "MO_BinaryParticle.h"

class MO_PSOPruner: public MOPruner {
    public:
        MO_PSOPruner(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
        virtual ~MO_PSOPruner();
        void Init(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
        void Reset(int np, int nt);
        void Prune(MTRand& mt);
        void clearVectors();
        void setParams(double c1, double c2, double c3, double c4, double w);
        double C1, C2, C3, C4, C5, C6, cFactor;
        double W;

    protected:
        void 	initPopulation(MTRand& mt);
        void 	evaluateFitness(MTRand& mt);
        void 	updatePositions(MTRand& mt);
        double 	sigMoid(double v);
        int iterations;

        std::vector <MO_BinaryParticle > swarm;
        std::vector < std::vector < int > > gBest;
        std::vector < double > gBestValues;
};

#endif /* MO_PSO_H_ */
